package githubop

import (
	"context"
	"errors"
	"log"
	"net/http"
	"os"

	"github.com/google/go-github/v50/github"
	"golang.org/x/oauth2"
)

var (
	ErrBaseURLRequired   = errors.New("base URL required")
	ErrUploadURLRequired = errors.New("upload URL required")
)

//go:generate go run github.com/maxbrunsfeld/counterfeiter/v6 -o fakes/gitoperator.go . GithubOperator

// GithubOperator is an interface for interacting with the GitHub API
type GithubOperator interface {
	PullRequestService
}

//go:generate go run github.com/maxbrunsfeld/counterfeiter/v6 -o fakes/prservice.go . PullRequestService

// PullRequestService is an interface for interacting with the GitHub Pull Request API
type PullRequestService interface {
	List(
		ctx context.Context,
		owner string,
		repo string,
		opts *github.PullRequestListOptions,
	) ([]*github.PullRequest, *github.Response, error)

	ListFiles(
		ctx context.Context,
		owner string,
		repo string,
		number int,
		opts *github.ListOptions,
	) ([]*github.CommitFile, *github.Response, error)

	Get(
		ctx context.Context,
		owner string,
		repo string,
		number int,
	) (*github.PullRequest, *github.Response, error)

	ListReviews(
		ctx context.Context,
		owner string,
		repo string,
		number int,
		opts *github.ListOptions,
	) ([]*github.PullRequestReview, *github.Response, error)
}

// GithubOp is a struct for interacting with the GitHub API
type GithubOp struct {
	client *github.Client
	token  string
	logger *log.Logger
}

// List returns a list of pull requests
func (g GithubOp) List(
	ctx context.Context,
	owner string,
	repo string,
	opts *github.PullRequestListOptions,
) ([]*github.PullRequest, *github.Response, error) {
	return g.client.PullRequests.List(ctx, owner, repo, opts)
}

// ListFiles returns a list of files for a given pull request
func (g GithubOp) ListFiles(
	ctx context.Context,
	owner string,
	repo string,
	number int,
	opts *github.ListOptions,
) ([]*github.CommitFile, *github.Response, error) {
	return g.client.PullRequests.ListFiles(ctx, owner, repo, number, opts)
}

func (g GithubOp) ListReviews(
	ctx context.Context,
	owner string,
	repo string,
	number int,
	opts *github.ListOptions,
) ([]*github.PullRequestReview, *github.Response, error) {
	return g.client.PullRequests.ListReviews(ctx, owner, repo, number, opts)
}

// Get returns a pull request
func (g GithubOp) Get(
	ctx context.Context,
	owner string,
	repo string,
	number int,
) (*github.PullRequest, *github.Response, error) {
	return g.client.PullRequests.Get(ctx, owner, repo, number)
}

// New returns a new GithubOp struct
func New(token string, ghURL string, ghUploadsURL string, client *http.Client, logger *log.Logger) (GithubOperator, error) {
	// sane token check
	if token == "" {
		token = os.Getenv("GITHUB_TOKEN")
		if token == "" {
			return nil, errors.New("GITHUB_TOKEN must be set")
		}
	}

	// sane GHE URL check this allows for a different URL to be used but defaults to the Comcast GHE REST API
	if ghURL == "" {
		return nil, ErrBaseURLRequired
	}

	// sane GHE Uploads URL check
	if ghUploadsURL == "" {
		return nil, ErrUploadURLRequired
	}

	// If the client is not provided, create a new one
	if client == nil {
		// create a new token source with our token
		ctx := context.TODO()
		ts := oauth2.StaticTokenSource(
			&oauth2.Token{AccessToken: token},
		)

		// create a new oauth2 Client with our token source
		tc := oauth2.NewClient(ctx, ts)

		// create a new GitHub Client utilizing the oauth2 Client
		client, err := github.NewEnterpriseClient(ghURL, ghUploadsURL, tc)
		if err != nil {
			return nil, err
		}

		return GithubOp{
			client: client,
			token:  token,
			logger: logger,
		}, nil

	}

	ghClient, err := github.NewEnterpriseClient(ghURL, ghUploadsURL, client)
	if err != nil {
		return nil, err
	}

	return &GithubOp{
		client: ghClient,
		token:  token,
		logger: logger,
	}, nil
}
