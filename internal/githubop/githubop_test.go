package githubop_test

import (
	"context"
	"github.com/google/go-github/v50/github"
	"gitlab.com/silenterror/go-github-pr/internal/githubop"
	"net"
	"net/http"
	"net/http/httptest"
	"testing"
)

// setupMockServer is a helper function for setting up a mock server and client for testing
func setupMockServer(responseBody string, responseStatus int) (*httptest.Server, *http.Client) {
	mockServer := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(responseStatus)
		w.Header().Set("Content-Type", "application/json")
		w.Write([]byte(responseBody))
	}))

	client := &http.Client{
		Transport: &http.Transport{
			DialContext: func(_ context.Context, _, _ string) (net.Conn, error) {
				return net.Dial("tcp", mockServer.Listener.Addr().String())
			},
		},
	}

	return mockServer, client
}

// assertListOutcome is a helper function for asserting the outcome of the List() function
func assertListOutcome(t *testing.T, prs []*github.PullRequest, err error, wantPrID int, wantErr bool, empty bool) {
	if (err != nil) != wantErr {
		t.Fatalf("List() error = %v, wantErr %v", err, wantErr)
	}

	if !wantErr {
		if empty && len(prs) != 0 {
			t.Errorf("List() expected no pull requests, got %v", len(prs))
		} else if !empty {
			if prs[0].ID == nil || *prs[0].ID != int64(wantPrID) {
				t.Errorf("List() got ID = %v, want ID %v", prs[0].ID, wantPrID)
			}
		}
	}
}

// TestGithubOp_List tests the List() function
func TestGithubOp_List(t *testing.T) {
	// Define test cases
	tests := []struct {
		name           string
		mockResponse   string
		wantPrID       int
		wantErr        bool
		responseStatus int
		empty          bool
	}{
		{
			name:           "success - single PR",
			mockResponse:   `[{"id": 1, "title": "Test Pull Request", "state": "open"}]`,
			wantPrID:       1,
			wantErr:        false,
			responseStatus: http.StatusOK,
		},
		{
			name:           "success - multiple PRs",
			mockResponse:   `[{"id": 1, "title": "Test Pull Request", "state": "open"}, {"id": 2, "title": "Test Pull Request 2", "state": "open"}]`,
			wantPrID:       1,
			wantErr:        false,
			responseStatus: http.StatusOK,
		},
		{
			name:           "success - no PRs",
			mockResponse:   `[]`,
			wantPrID:       0,
			wantErr:        false,
			responseStatus: http.StatusOK,
			empty:          true,
		},
		{
			name:           "failure - bad response",
			mockResponse:   `{"message": "Not Found", "documentation_url": "https://docs.github.com/rest/reference/pulls#list-pull-requests"}`,
			wantPrID:       0,
			wantErr:        true,
			responseStatus: http.StatusNotFound,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			mockServer, client := setupMockServer(tt.mockResponse, tt.responseStatus)
			defer mockServer.Close()

			ghOp, err := githubop.New("some_secret_token", mockServer.URL, mockServer.URL, client, nil)
			if err != nil {
				t.Fatalf("New() error = %v", err)
			}

			prs, _, err := ghOp.List(context.Background(), "owner", "repo", nil)
			assertListOutcome(t, prs, err, tt.wantPrID, tt.wantErr, tt.empty)
		})
	}
}
