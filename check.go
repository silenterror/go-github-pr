package pullrequests

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/google/go-github/v50/github"
	"gitlab.com/silenterror/go-github-pr/internal/githubop"
	"gitlab.com/silenterror/go-github-pr/models"
	"log"
	"os"
	"regexp"
)

// Checker is a struct for checking for new versions of the resource
type Checker struct {
	github                  githubop.GithubOperator
	states                  []string
	disableForks            bool
	ignoreDrafts            bool
	requiredReviewApprovals int
	repoName                string
	repoOwner               string
}

// New returns a new Checker
func New(
	ghOp githubop.GithubOperator,
	states []string,
	disableForks bool,
	ignoreDrafts bool,
	requiredReviewApprovals int,
	repoName string,
	repoOwner string,
) *Checker {
	return &Checker{
		github:                  ghOp,
		states:                  states,
		disableForks:            disableForks,
		ignoreDrafts:            ignoreDrafts,
		requiredReviewApprovals: requiredReviewApprovals,
		repoName:                repoName,
		repoOwner:               repoOwner,
	}
}

// Check is the entrypoint for the Concourse check resource
func (c *Checker) Check() ([]models.PullRequestInfo, error) {
	var prInfos []models.PullRequestInfo

	// iterate over the states and get the pull requests
	for _, state := range c.states {
		prs, _, err := c.github.List(
			context.TODO(),
			c.repoOwner,
			c.repoName,
			&github.PullRequestListOptions{State: state},
		)
		if err != nil {
			return nil, fmt.Errorf("failed to list pull requests: %w", err)
		}

		// iterate over the prs, filter them by the options, and add them to the response
		for _, pr := range prs {
			if c.disableForks && pr.GetHead().GetRepo().GetFork() {
				continue
			}

			if c.ignoreDrafts && pr.GetDraft() {
				continue
			}

			if containsSkipCI(pr.GetTitle()) || containsSkipCI(pr.GetBody()) {
				continue
			}

			// Assuming you have a function to get the approver count for a PR
			approverCount, err := c.GetApproverCount(pr)
			if err != nil {
				log.Printf("error getting approver count for PR #%d: %s", pr.GetNumber(), err)
				continue
			}

			// Create a PullRequestInfo object for each PR that passes the filters
			prInfo := models.PullRequestInfo{
				Number:        pr.GetNumber(),
				State:         pr.GetState(),
				CommitHash:    pr.GetHead().GetSHA(),
				CommittedDate: pr.GetCreatedAt().Time, // Or use another appropriate date field
				ApproverCount: approverCount,
			}
			prInfos = append(prInfos, prInfo)
		}
	}

	// Output the PR information to stdout in JSON format
	json.NewEncoder(os.Stdout).Encode(prInfos)

	return prInfos, nil
}

// getApproverCount returns the number of approvals for a given PR
func (c *Checker) GetApproverCount(pr *github.PullRequest) (int, error) {
	// Get the reviews for the PR
	reviews, _, err := c.github.ListReviews(
		context.TODO(),
		c.repoOwner,
		c.repoName,
		pr.GetNumber(),
		nil,
	)
	if err != nil {
		return 0, fmt.Errorf("failed to list reviews: %w", err)
	}

	// Iterate over the reviews and count the approvals
	approverCount := 0
	for _, review := range reviews {
		if review.GetState() == "APPROVED" {
			approverCount++
		}
	}

	return approverCount, nil
}

// containsSkipCI returns true if the commit message contains [ci skip] or [skip ci]
func containsSkipCI(s string) bool {
	re := regexp.MustCompile("(?i)\\[(ci skip|skip ci)]")
	return re.MatchString(s)
}
