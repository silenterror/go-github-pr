package models

import "time"

// PullRequestInfo is the response body for the Concourse out resource
type PullRequestInfo struct {
	Number        int       `json:"number"`
	State         string    `json:"state"`
	CommitHash    string    `json:"commit_hash"`
	CommittedDate time.Time `json:"committed_date"`
	ApproverCount int       `json:"approver_count"`
}
