package main

import (
	"encoding/json"
	"errors"
	"log"
	"os"
	"strconv"
	"strings"

	pullrequests "gitlab.com/silenterror/go-github-pr"
	"gitlab.com/silenterror/go-github-pr/internal/githubop"
)

var (
	githubAPIURL      = os.Getenv("GITHUB_API_URL")
	githubUploadsURL  = os.Getenv("GITHUB_UPLOADS_URL")
	prStatus          = getEnvAsSlice("PR_STATUS", "open")
	prDisableForks    = getEnvAsBool("PR_DISABLE_FORKS", false)
	prIgnoreDrafts    = getEnvAsBool("PR_IGNORE_DRAFTS", false)
	prRequiredReviews = getEnvAsInt("PR_REQUIRED_REVIEWS", 0)
	githubToken       = os.Getenv("GITHUB_TOKEN")
	repoName          = os.Getenv("REPO_NAME")
	repoOwner         = os.Getenv("REPO_OWNER")
)

func main() {
	if len(os.Args) < 2 {
		log.Fatal("An operation must be specified.")
	}
	operation := os.Args[1]

	// validate environment variables
	if err := validateEnv(); err != nil {
		log.Fatal(err)
	}

	// Create a GitHub client
	ghClient, err := githubop.New(
		githubToken,
		githubAPIURL,
		githubUploadsURL,
		nil,
		log.New(os.Stderr, "", log.LstdFlags),
	)
	if err != nil {
		log.Fatalf("Failed to create GitHub client: %s", err)
	}

	// Create a new checker
	checker := pullrequests.New(
		ghClient,
		prStatus,
		prDisableForks,
		prIgnoreDrafts,
		prRequiredReviews,
		repoName,
		repoOwner,
	)

	switch operation {
	case "check":
		prs, err := checker.Check()
		if err != nil {
			log.Fatalf("Failed to check for new versions: %s", err)
		}

		// Output the pull requests in the format expected by Concourse
		if err := json.NewEncoder(os.Stdout).Encode(prs); err != nil {
			log.Fatalf("Failed to encode the response: %s", err)
		}

	case "in":
		// Call the in operation
		log.Println("NOT YET IMPLEMENTED")
	case "out":
		log.Println("NOT YET IMPLEMENTED")
	default:
		log.Fatalf("Unknown operation %s", operation)
	}
}

func validateEnv() error {
	if githubAPIURL == "" {
		return errors.New("GITHUB_API_URL must be set")
	}
	if githubUploadsURL == "" {
		return errors.New("GITHUB_UPLOADS_URL must be set")
	}
	if githubToken == "" {
		return errors.New("GITHUB_TOKEN must be set")
	}
	return nil
}

func getEnvAsSlice(key string, defaultValue string) []string {
	value := os.Getenv(key)
	if value == "" {
		return []string{defaultValue}
	}
	return strings.Split(value, ",")
}

func getEnvAsBool(key string, defaultValue bool) bool {
	value := os.Getenv(key)
	if value == "" {
		return defaultValue
	}
	boolValue, err := strconv.ParseBool(value)
	if err != nil {
		log.Fatalf("Invalid boolean for %s: %v", key, err)
	}
	return boolValue
}

func getEnvAsInt(key string, defaultValue int) int {
	value := os.Getenv(key)
	if value == "" {
		return defaultValue
	}
	intValue, err := strconv.Atoi(value)
	if err != nil {
		log.Fatalf("Invalid integer for %s: %v", key, err)
	}
	return intValue
}
